
% This script is valid only for a calibrated measurement (i.e. x is in pascal) 

% To use, load a converted mat file and then run this script

% Allows to change the window size
windowSize = 4096*4;

% This estimates the power spectrum of the signal over the whole
% measurement
[pxx,f] = pwelch(x,hanning(windowSize),windowSize/2,windowSize,fs);

hold on;
semilogx(f,10*log10(pxx/((20*10^-6)^2)))
title('PSD of the loaded signal')
legend('PSD')
xlabel('Frequency (Hz)')
ylabel('PSD (dB/Hz)')