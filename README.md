# MEC636 to MATLAB tool

This is a set of scripts that allow to convert the output of the MEC636 saved text file to Matlab .mat.

There is also a few example scripts for the analysis of the sound spectrums

## Analysis

pwelch, poctave are useful function that can be used for the analysis of the signals
