% This script works on an OSX machine you might need to change things

% To use simply place all your MEC636 measurement in a "ToConvert" folder.
% All the files will be converted to matlab .mat files

clearvars;

% Setup the script
flag = 0; % Used to not start the algorithm in case of problems
if ~exist('ToConvert', 'dir')
    flag = 1;
    mkdir('ToConvert');
end

if ~exist('Converted', 'dir')
    mkdir('Converted');
end

% Finds all the files in the folder
files = dir('ToConvert');
dirFlags = [files.isdir];
files = files(~dirFlags);

if isempty(files)
    flag = 1;
end


if flag==1
    fprintf("Please use the folders ToConvert in order to convert\n");
    return 
end


for i = 1 : length(files)

    fileName = files(i).name;
    fileID = fopen(strcat('ToConvert/',fileName),'r');

    %Ignore first three lines
    fgetl(fileID);
    fgetl(fileID);
    fgetl(fileID);

    %Sesnsibilite fabriquant
    string = fgetl(fileID);
    string = string(32:end);
    string = strrep(string,',','.');
    sf = str2double(string);

    %Sesnsibilite fabriquant
    string = fgetl(fileID);
    string = string(34:end);
    string = strrep(string,',','.');
    se = str2double(string);

    %Fréquence d'échantillonnage
    string = fgetl(fileID);
    string = string(36:end);
    string = strrep(string,',','.');
    fs = str2double(string);

    %Skip two lines
    fgetl(fileID);
    fgetl(fileID);

    % Converts the data
    string = textscan(fileID,'%s');
    string = vertcat(string{:});
    string = strrep(string,',','.');
    x = str2double(string);
    
    fclose('all');

    matName = textscan(fileName,'%s','delimiter','.');
    matName = vertcat(matName{:});
    save(strcat('Converted/',matName{1}),'x','se','sf','fs');
end











