close all;

% This script is valid only for a calibrated measurement (i.e. x is in pascal) 

% To use, load a converted mat file and then run this script

% Allows to change the window size
bands = 1;

% This estimates the power spectrum of the signal over the whole
% measurement
[poct, fc] = poctave(x,fs,'BandsPerOctave',bands);

hold on;
bar(10*log10(poct/((20*10^-6)^2)))
xlabel('Frequency (Hz)');
ylabel('Amplitude (dBSPL)');
set(gca,'XTick',1:length(fc));
set(gca,'XTickLabel',fc);
xtickangle(45);